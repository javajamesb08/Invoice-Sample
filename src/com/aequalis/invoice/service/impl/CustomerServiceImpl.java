package com.aequalis.invoice.service.impl;

import com.aequalis.invoice.dao.CustomerDao;
import com.aequalis.invoice.dao.OrderDao;
import com.aequalis.invoice.dao.ProductDao;
import com.aequalis.invoice.domain.Customer;
import com.aequalis.invoice.domain.Order;
import com.aequalis.invoice.domain.Product;
import com.aequalis.invoice.service.CustomerService;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.*;

/**
 * Created by jamesanto on 7/7/16.
 */
public class CustomerServiceImpl implements CustomerService{
    private CustomerDao customerDao;
    private ProductDao productDao;
    private OrderDao orderDao;

    public CustomerServiceImpl(CustomerDao customerDao, ProductDao productDao, OrderDao orderDao) {
        this.customerDao = customerDao;
        this.productDao = productDao;
        this.orderDao = orderDao;
    }

    @Override
    public void add(Customer customer) {
        customerDao.add(customer);
    }

    @Override
    public void update(Customer customer) {

    }

    @Override
    public void delete(Long customerId) {

    }

    @Override
    public Customer view(Long customerId) {
        return null;
    }

    @Override
    public List<Customer> listAll() {
        return null;
    }

    @Override
    public Set<Product> listProducts(Long customerId) {
        List<Order> orders = orderDao.listOrders();
        List<Order> myOrders = new ArrayList<>();
        for(Order order : orders) {
            if(order.getCustomer().getId().equals(customerId)) {
                myOrders.add(order);
            }
        }
        Set<Product> myProducts = new HashSet<>();
        for(Order order : myOrders) {
            myProducts.add(order.getProduct());
        }
        return myProducts;
    }

    @Override
    public Map<Product, Long> listProductsWithQuantity(Long customerId) {
        List<Order> orders = orderDao.listOrders();
        List<Order> myOrders = new ArrayList<>();
        for(Order order : orders) {
            if(order.getCustomer().getId().equals(customerId)) {
                myOrders.add(order);
            }
        }
        Map<Product, Long> myProducts = new HashMap<>();
        for(Order order : myOrders) {

            Long quantity = 0L;

            Long existingProductCount = myProducts.get(order.getProduct());
            if(existingProductCount == null) {
                quantity = order.getQuantity();
            } else {
                quantity = order.getQuantity() + existingProductCount;
            }

            myProducts.put(order.getProduct(), quantity);
        }
        return myProducts;
    }

    @Override
    public void buyProduct(Long customerId, Long productId, Long quantity) {
        //Product product = producatDao.getPro(
        //product.setQuantity(product.getQuantity - quantity)
        //productDao.update(product)

        Customer customer = customerDao.view(customerId);
        Product product = productDao.view(productId);
        Order order = new Order();
        order.setCustomer(customer);
        order.setProduct(product);
        order.setQuantity(quantity);
        orderDao.add(order);
    }
}
