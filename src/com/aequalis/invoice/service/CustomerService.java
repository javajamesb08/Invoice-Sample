package com.aequalis.invoice.service;

import com.aequalis.invoice.domain.Customer;
import com.aequalis.invoice.domain.Product;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by jamesanto on 7/7/16.
 */
public interface CustomerService {
    void add(Customer customer);

    void update(Customer customer);

    void delete(Long customerId);

    Customer view(Long customerId);

    List<Customer> listAll();

    Set<Product> listProducts(Long customerId);

    /**
     * pr1 - 10
     * pr2 - 5
     * pr1 - 5
     *
     * pr1 - 15
     * pr2 - 5
     * @param customerId
     * @return
     */
    Map<Product, Long> listProductsWithQuantity(Long customerId);

    void buyProduct(Long customerId, Long productId, Long quantity);
}
