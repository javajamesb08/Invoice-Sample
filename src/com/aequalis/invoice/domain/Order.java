package com.aequalis.invoice.domain;

import java.util.List;
import java.util.Map;

/**
 * Created by jamesanto on 7/7/16.
 */

//customer_id, product_id, quantity
public class Order {
    private Long id;
    private Customer customer;
    private Map<Product, Long> products;



    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
