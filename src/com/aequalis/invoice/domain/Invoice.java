package com.aequalis.invoice.domain;

import java.util.Map;

/**
 * Created by jamesanto on 7/7/16.
 */
public class Invoice {
    private Long id;
    private Order order;
    private Map<Product, Long> productPrice;
}
