package com.aequalis.invoice.domain;

/**
 * Created by jamesanto on 7/7/16.
 */
public class Product {
    private Long id;
    private String name;
    private Double price;
    private Long quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
