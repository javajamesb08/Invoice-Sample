package com.aequalis.invoice.dao;

import com.aequalis.invoice.domain.Customer;

import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public interface CustomerDao {
    void add(Customer customer);

    void update(Customer customer);

    void delete(Long customerId);

    Customer view(Long customerId);

    List<Customer> listAll();
}
