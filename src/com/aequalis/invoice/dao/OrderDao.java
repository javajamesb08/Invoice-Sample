package com.aequalis.invoice.dao;

import com.aequalis.invoice.domain.Order;

import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public interface OrderDao {
    void add(Order order);
    public List<Order> listOrders();
}
