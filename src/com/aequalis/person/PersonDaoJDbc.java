package com.aequalis.person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public class PersonDaoJDbc implements PersonDao{
    public List<Person> list() {
        List<Person> list = new ArrayList<>();
        list.add(new Person("John", 10));
        list.add(new Person("Doe", 14));
        return list;
    }
}
