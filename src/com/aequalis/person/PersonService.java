package com.aequalis.person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public class PersonService {
    private PersonDao personDao;
    public PersonService(PersonDao personDao) {
        this.personDao = personDao;
    }

    public List<Person> getTeenagePersons() {
        List<Person> teenagePersons = new ArrayList<>();
        List<Person> allPersons = personDao.list();
        for(Person person : allPersons) {
            if(person.getAge() >= 13 && person.getAge() <= 19) {
                teenagePersons.add(person);
            }
        }
        return teenagePersons;
    }
}
