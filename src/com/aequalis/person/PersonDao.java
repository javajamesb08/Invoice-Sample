package com.aequalis.person;

import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public interface PersonDao {
    List<Person> list();
}
