package com.aequalis.person;

import java.util.List;

/**
 * Created by jamesanto on 7/7/16.
 */
public class Main {
    public static void main(String[] args) {
        PersonDaoJDbc personDaoJDbc = new PersonDaoJDbc();
        PersonDaoFile personDaoFile = new PersonDaoFile();
        
        PersonService personService = new PersonService(personDaoFile);

        List<Person> teenage = personService.getTeenagePersons();
    }
}
